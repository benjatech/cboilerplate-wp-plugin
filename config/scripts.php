<?php
/**
  *
  * Scripts to Load
  *
  * How to use:
  *
  * Copy your scripts files to assets/script
  * 
  * 1) Loading scripts in all pages
  * $scripts = [
  *             ['script_name', 'script1.css']
  *             ['script_name2', 'script2.css']
  *           ];
  *
  * 2) Loading on specifi pages
  * $scripts = ['admin' => ['script_name', 'script1.js']]; (Only will load on Admin page)
  * $scripts = ['user'  => ['script_name2', 'script2.js']]; (Only will load on user page (Site front page))
  *
  * 3) Combine them to load diferent scripts in different parts
  *
  *	$scripts = [
  *				'both' 	=> ['script', 'script.js'],
  *				'admin' => ['script_admin', 'script_admin.js'],
  *				'user' 	=> ['script_user', 'script_user.js']
  *				];
  *
  *  This code will load script.js in all pages, script_admin.js on admin pages and
  *  script_user.js in user pages
  *
  * 4) To load external sctips, include all path
  * 
  * $scripts = [
  *             ['script_name', 'script.js']
  *             ['bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js']
  *           ];
  *
  * 5) If you want to add more info (https://developer.wordpress.org/reference/functions/wp_enqueue_script/) to your script:
  *
  *	$scripts = [[handler, src, dependencies, version, in_footer]];
  * 
  **/

	$scripts = [
				'both'  => ['plugin_slug_both_script', 'plugin_slug_script.js', array(), false, false, false],
				'admin' => ['plugin_slug_script_admin', 'plugin_slug_script_admin.js', array(), false, false, false],
				'user'  => ['plugin_slug_script_admin', 'plugin_slug_script_user.js', array(), false, false, false]
				];
	 
