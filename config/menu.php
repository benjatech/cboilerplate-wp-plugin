<?php
/**
  *
  * Menu Information
  *
  * To create your menu:
  *
  * Main Page:
  *
  *	$menu['MainPage'] = [
  *		'page_title'   	=> 'Page Title', 	#1
  *		'menu_title'   	=> 'plugin_name, 	#1
  *		'capability' 	=> 'manage_options',#1 
  *		'menu_slug'    	=> 'plugin_name', 	#1
  *		'icon_url'	   	=> null, 			#1
  *		'position'	   	=> null, 			#1
  *		'controller'	=> 'controller', 	#2
  *		'function'	 	=> 'function', 		#2
  *		'option_group' 	=> 'plugin_name_option_group', 	#3
  *		'option_name'  	=> 'plugin_name_option_name' 	#3
  *	];
  *
  * #1 Visit https://developer.wordpress.org/reference/functions/add_menu_page/
  *
  * #2 Insert a value for controller and function if you don't want to use the default values
  *
  * Default values:
  * 		controller 	=> Menu array key (in this sample, it is MainPage)
  * 		function	=> render_page
  *
  * 	Only set controller if you don't want to storage values on the page
  *
  *  #3 Fill if you want to store values to use in your plugin
  *
  * Sub Page:
  *
  * #1 https://developer.wordpress.org/reference/functions/add_submenu_page/
  *	'parent_slug' must be the same as 'menu_slug' from the Main Page.
  *
  *	#2 and #3 follow the same principle from the Main Page
  * 
  *
  **/

	$menu['MainPage'] = [
		'page_title'   	=> 'Plugin_Name',
		'menu_title'   	=> 'Plugin_Name',
		'capability' 	=> 'manage_options',
		'menu_slug'    	=> 'plugin_slug',
		'icon_url'	   	=> null,
		'position'	   	=> null,
		'option_group' 	=> 'plugin_slug_option_group',
		'option_name'  	=> 'plugin_slug_option_name'
	];
	
	$menu['Page1'] = [
		'parent_slug'  	=> 'plugin_slug',
		'page_title'   	=> 'Plugin_Name Sub 1',
		'menu_title'   	=> 'Plugin_Name Sub 1',
		'capability'   	=> 'manage_options',
		'menu_slug'    	=> 'plugin_slug_settings',
		'option_group' 	=> 'plugin_slug_option_group1',
		'option_name'  	=> 'plugin_slug_option_name1'
	];
	
	$menu['Page2'] = [
		'parent_slug'  	=> 'plugin_slug',
		'page_title'   	=> 'Plugin_Name Sub 2',
		'menu_title'   	=> 'Plugin_Name Sub 2',
		'capability'   	=> 'manage_options',
		'menu_slug'   	=> 'plugin_slug_settings2',
		'option_group' 	=> 'plugin_slug_option_group2',
		'option_name'  	=> 'plugin_slug_option_name2'
	];
