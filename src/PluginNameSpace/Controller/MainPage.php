<?php
namespace PluginNameSpace\Controller;

use PluginNameSpace\Core\Lib\Load;
use PluginNameSpace\Core\Lib\Page;
use PluginNameSpace\Core\Lib\General;

class MainPage extends Page {
	
    public function render_page() {

	    Load::view('MainPage');
		
	}
	
}
