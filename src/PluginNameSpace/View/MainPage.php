<div class="wrap">
    <h2><?=_e('Plugin_Name', PLUGINDEFINE_LANG);?></h2>
    <p><?=_e('About this page', PLUGINDEFINE_LANG);?></p>
    <div class="cboiler-box">
        <?=$helper->form()->open(['class' => 'cboiler-form']);?>
            <fieldset>
                <section>
                    <?=$helper->form()->label('@.text', __('Text', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="input">
                        <?=$helper->form()->text('@.text', $helper->get('text'), ['id' => 'text', 'placeholder' => __('Text', PLUGINDEFINE_LANG)]);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.text');?></strong> => <?=$helper->get('text');?>
                    </div>
                </section>
                <section>
                    <?=$helper->form()->label('@.password', __('Password', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="input">
                        <?=$helper->form()->password('@.password', $helper->get('password'), ['id' => 'password', 'placeholder' => __('Password', PLUGINDEFINE_LANG)]);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.password');?></strong> => <?=$helper->get('password');?>
                    </div>
                </section>
                <section>
                    <?=$helper->form()->label('@.checkbox', __('Checkbox', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="checkbox">
                        <?=$helper->form()->checkbox('@.check', 1, $helper->get('check'), ['id' => 'check']);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.check');?></strong> => <?=$helper->get('check');?>
                    </div>
                </section>
                <section>
                    <?=$helper->form()->label('@.radio', __('Radio', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="radio">
                        <?=$helper->form()->radio('@.radio', 'value 1', $helper->get('radio'), ['id' => 'radio1']);?>
                        <?=_e('Value 1', PLUGINDEFINE_LANG);?>
                    </label>
                    <label class="radio">
                        <?=$helper->form()->radio('@.radio', 'value 2', $helper->get('radio'), ['id' => 'radio2']);?>
                        <?=_e('Value 2', PLUGINDEFINE_LANG);?>
                    </label>
                    <label class="radio">
                        <?=$helper->form()->radio('@.radio', 'value 3', $helper->get('radio'), ['id' => 'radio3']);?>
                        <?=_e('Value 3', PLUGINDEFINE_LANG);?>
                    </label>
                    <div class="note">
                        Current Value: <strong><?=$helper->form()->name('@.radio');?></strong> => <?=$helper->get('radio');?>
                    </div>
                </section>
                <section>
                    <?=$helper->form()->label('@.textarea', __('Textarea', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="textarea">
                        <?=$helper->form()->textarea('@.textarea', $helper->get('textarea'), ['id' => 'textarea', 'placeholder' => __('Textarea', PLUGINDEFINE_LANG), 'rows' => 5]);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.textarea');?></strong> => <?=$helper->get('textarea');?>
                    </div>
                </section>
                <section>
                    <?=$helper->form()->label('@.select', __('Select', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="select">
                        <?=$helper->form()->select('@.select', ['value1' => __('Value 1', PLUGINDEFINE_LANG),'value2' => __('Value 2', PLUGINDEFINE_LANG),'value3' => __('Value 3', PLUGINDEFINE_LANG)], $helper->get('select'), ['id' => 'select']);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.select');?></strong> => <?=$helper->get('select');?>
                    </div>
                </section>
            </fieldset>
            <footer>
                <?=$helper->form()->submit(__('Save Options', PLUGINDEFINE_LANG), ['class' => 'btn btn-blue']);?>
            </footer>
            
        <?=$helper->form()->close();?>
    </div>
</div>