<?php
namespace PluginNameSpace\Core\Helper;

use PluginNameSpace\Core\Helper\Helper;

class Alert extends Helper {

    /**
     * @var $type Define type of the alert (success, error, warning, info)
     */
	private $type;
    /**
     * @var $text_strong Define the text that will be wrapped in strong tags
     */
	private $text_strong;
    /**
     * @var $text_main Define the main text of alert
     */
	private $text_main;
    /**
     * @var $has_close Define if the alert is dismissible
     */
	private $has_close;

    /**
     * Alert constructor.
     *
     * @param string|bool $type
     * @param string|bool $text
     * @param bool $has_close
     */
	public function __construct($type = false, $text = false, $has_close = true) {

		$this->set_type($type);
		$this->set_text($text);
		$this->set_has_close($has_close);

	}
    /**
     * Set Type 
     * 
     * @param $type
     */
	private function set_type($type) {

		$this->type = strtolower($type);

	}
	
    /**
     * Set Text
     * 
     * @param $text
     */
	private function set_text($text) {
		
		$separate = explode('|', $text);
		
		if (count($separate) >= 2) {
			$this->text_strong = $separate[0];
			$this->text_main = $separate[1];
		} else {
			$this->text_strong = ucfirst($this->type);
			$this->text_main = $text;
		}

	}
	
    /**
     * Set Has Close
     * 
     * @param $has_close
     */
	private function set_has_close($has_close) {

		$this->has_close = $has_close;

	}
	
    /**
     * Create Alert
     * 
     * @param bool $echo
     *
     * @return string
     */
	public function show($echo = true) {
		
		$output = "<div class='cboiler-alert cboiler-alert-{$this->type}'>";
		$output .= "<button class='cboiler-close' data-dismiss='alert'>×</button>";
		$output .= "<strong>{$this->text_strong}</strong> ";
		$output .= $this->text_main;
		$output .= '</div>';
		
		if ($this->has_close) {
			$output .= '<script>';
			$output .= 'jQuery(".cboiler-alert .cboiler-close").click(function(e) {';
			$output .= '	e.preventDefault();';
			$output .= '	jQuery(this).parent().slideUp();';
			$output .= '});';
			$output .= '</script>';
		}
		
		if ($echo) echo $output;
		else return $output;
	
	}

    /**
     * Show Wordpress standard alert
     * 
     * @param bool $echo
     *
     * @return string
     */
	public function wordpress($echo = true) {
		
		$output = "<div id='setting-error-settings_updated' class='updated settings-error notice is-dismissible cboiler-alert-wp-{$this->type}'>";
		$output .= "<p><strong>{$this->text_strong}</strong> {$this->text_main}</p>";
		$output .= "</div>";
		
		if ($echo) echo $output;
		else return $output;
		
	}
	
	
}