<?php
namespace PluginNameSpace\Core\Helper;

use PluginNameSpace\Core\Helper\Helper;
use PluginNameSpace\Core\Helper\Alert;

class Form extends Helper {

    /**
     * @var $helper
     */
    private $helper;

    /**
     * Form constructor.
     *
     * @param $instance
     */
    function __construct($instance) {

        $this->helper = $instance;

    }

    /**
     * Open Attributes array to string
     *
     * @param array $attributes
     *
     * @return bool|string
     */
    private function attributes($attributes) {

        if (!is_array($attributes)) return false;

        $output = '';

        foreach ($attributes as $att => $value) {
            $output .= "{$att} = '{$value}' ";
        }

        return $output;

    }

    /**
     * Treat name entrance
     *
     * @param string $name
     *
     * @return string
     */
    public function name($name) {

        $parts = explode('.', $name);

        if (!is_array($parts) || count($parts) === 1) return $name;

        $output = '';
        foreach ($parts as $part) {
            switch ($part) {
                case '@': $output .= $this->helper->get_option_name(); break;
                case '?': $output .= "[]"; break;
                default : $output .= "[{$part}]";
            }
        }

        return $output;

    }
    /**
     * Insert an Input element
     *
     * @param $type (text, password, checkbox, radio)
     * @param string $name
     * @param string|bool $value
     * @param array|bool $attr
     *
     * @return string
     */
    private function input($type, $name, $value = false, $attr = false) {

        $output = "<input type='{$type}' name='{$this->name($name)}' value='{$value}' {$this->attributes($attr)} />";
        return $output;

    }

    /**
     * Insert an Input Text element
     *
     * @param string $name
     * @param string|bool $value
     * @param array|bool $attr
     *
     * @return string
     */
    public function text($name, $value = false, $attr = false) {

        return $this->input('text', $name, $value, $attr);

    }

    /**
     * Insert an Input Password element
     *
     * @param string $name
     * @param string|bool $value
     * @param array|bool $attr
     *
     * @return string
     */
    public function password($name, $value = false, $attr = false) {

        return $this->input('password', $name, $value, $attr);

    }

    /**
     * Insert a Checkbox element
     *
     * @param string $name
     * @param string $value
     * @param strin|bool $current
     * @param array|bool $attr
     *
     * @return string
     */
    public function checkbox($name, $value, $current = false, $attr = false) {

        if ($current == $value) $attr['checked'] = 'checked';

        return $this->input('checkbox', $name, $value, $attr);

    }

    /**
     * Insert a Radio element
     *
     * @param string $name
     * @param string $value
     * @param strin|bool $current
     * @param array|bool $attr
     *
     * @return string
     */
    public function radio($name, $value, $current = false, $attr = false) {

        if ($current == $value) $attr['checked'] = 'checked';

        return $this->input('radio', $name, $value, $attr);

    }

    /**
     * Insert a Textarea element
     *
     * @param string $name
     * @param bool $value
     * @param bool $attr
     *
     * @return string
     */
    public function textarea($name, $value = false, $attr = false) {

        $output = "<textarea name='{$this->name($name)}' {$this->attributes($attr)} />{$value}</textarea>";

        return $output;

    }

    /**
     * Insert a Select element
     *
     * @param string $name
     * @param array $options
     * @param string|bool $current
     * @param array|bool $attr
     *
     * @return bool|string
     */
    public function select($name, $options, $current = false, $attr = false) {

        if (!is_array($options)) return false;

        $output = "<select name='{$this->name($name)}' {$this->attributes($attr)}>";

        foreach ($options as $key => $option) {
            $selected = ($key == $current)?'selected':'';

            $output .= "<option value='{$key}' {$selected}>{$option}</option>";
        }

        $output .= "</select>";

        return $output;

    }

    /**
     * Insert a Label element
     *
     * @param string $for
     * @param string $text
     * @param array|bool $attr
     *
     * @return string
     */
    public function label($for, $text, $attr = false) {

        return "<label for='{$this->name($for)}' {$this->attributes($attr)}>{$text}</label>";

    }

    /**
     *  Open Form
     *
     * @param array|bool $attr
     * @param bool $cboiler_alert
     *
     */
    public function open($attr = false, $cboiler_alert = true) {

        echo "<form method='post' action='options.php' {$this->attributes($attr)}>";

        if (isset($_GET['settings-updated'])) {
            $alert = new Alert('success', __('Settings saved!', PLUGINDEFINE_LANG));
            ($cboiler_alert)? $alert->show():$alert->wordpress();
        }

        settings_fields($this->helper->get_option_group());
        do_settings_sections($this->helper->get_option_group());

    }

    /**
     * Close Form
     *
     * @return string
     */
    public function close() {

        return '</form>';

    }

    /**
     * Insert a Button Element
     *
     * @param string $name
     * @param string $value
     * @param array|bool $attr
     *
     * @return string
     */
    public function button($name, $value, $attr = false) {

        return $this->input('button', $name, $value, $attr);

    }

    /**
     * Insert a Submit Button Element
     *
     * @param string $value
     * @param array|bool $attr
     *
     * @return string
     */
    public function submit($value, $attr = false) {

        return $this->input('submit', 'submit', $value, $attr);

    }



}