<?php
namespace PluginNameSpace\Core\Helper;

use PluginNameSpace\Core\Helper\Form;

class Helper {
	
	private $option_name;

	private $option_group;
	
	private $option_values;
	
	/* Set Option Name
	 * 
     * @return void
     *
     */
	public function set_option_name($option) {

		$this->option_name = $option['name'];

	}
	
	/* Get Option Name
	 * 
     * @return void
     *
     */
	public function get_option_name() {

		return $this->option_name;

	}

	/* Set Option Group
	 * 
     * @return void
     *
     */
	public function set_option_group($option) {

		$this->option_group = $option['group'];

	}
	
	/* Get Option Group
	 * 
     * @return void
     *
     */
	public function get_option_group() {

		return $this->option_group;

	}
	
	/* Set Option Values
	 * 
     * @return void
     *
     */
	public function set_option_values($option_values) {

		$this->option_values = $option_values;

	}
	
	/* Get Option Value
	 * 
     * @return void
     *
     */
	public function get_option_value($option_value) {
		
		$parts = explode('.', $option_value);
		
		if (!is_array($parts) || count($parts) === 1) {
			$option = (isset($this->option_values[$option_value]))?$this->option_values[$option_value]:false;
		} else {
			$option = $this->option_values;
			foreach ($parts as $part) {
				if (!isset($option[$part])) return false;
				$option = $option[$part];
			}
		}
		
		return $option;

	}
	
	/* Get Option Value (alias to get_option_value)
	 * 
     * @return void
     *
     */
	public function get($option_value) {

		return $this->get_option_value($option_value);

	}
		
	/* Call Form Helper
	 * 
     * @return Form Class
     *
     */
    public function form() {

		return new Form($this);

	}
	
	/* Call Alert Helper
	 * 
     * @return Alert Class
     *
     */
    public function alert($type = false, $text = false, $has_close = true) {

		return new Alert($type, $text, $has_close);

	}
}

