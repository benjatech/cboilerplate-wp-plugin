<?php
namespace PluginNameSpace\Core\Lib;

class Controller {

	/* Get Controller Namespace
	 * 
     * @param $controller string
     *
     * @return string
     *
     */
    public function get_namespace($controller) {
		return '\\'.PLUGINDEFINE_NAMESPACE.'\Controller\\'.$controller;
	}

	/* create a new instance of Controller
	 * 
     * @param $controller string
     *
     * @return instance
     *
     */
    public function do_new($controller) {
		$class = '\\'.PLUGINDEFINE_NAMESPACE.'\Controller\\'.$controller;
		return New $class;
	}

}
