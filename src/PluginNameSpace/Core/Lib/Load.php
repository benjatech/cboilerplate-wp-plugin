<?php
namespace PluginNameSpace\Core\Lib;

use PluginNameSpace\Core\Helper\Helper;
use PluginNameSpace\Core\Lib\Page;

class Load {
	
    /*
     * Return config variables
     *
     * Open file on config directory and require it once
     *
     * If filename is the same as the variable inside config file
     * then let var as false. But, if the variable is different
     * then you must include the $var value.
     *
     * @param $file string
     * @param $var bool|string
     *
     * @return mixed
     */
	public static function config($file, $var = false) {
	
		$path = PLUGINDEFINE_PATH .'config' . DIRECTORY_SEPARATOR . $file.'.php';
		
		if (file_exists($path)) {

			require PLUGINDEFINE_PATH .'config' . DIRECTORY_SEPARATOR . $file.'.php';
	
			return (!$var)?$$file:$$var;
		} else return false;
	
	}

    /*
     * General function do get full path of file
     *
     * @param $file string
     * @param $directory string (View or Model or Controller)
     * @param $extension (Only used on View case)
     *
     * @return string
     */	
	private static function get_file_path($file, $directory, $extension = 'php') {
		
		return PLUGINDEFINE_PATH . 'src' . DIRECTORY_SEPARATOR . PLUGINDEFINE_NAMESPACE . '/' . $directory . '/' . $file . '.' . $extension;
	
	}
	
    /*
     * Open an View file
     *
     * @param $file string
     *
     * @return void
     */	

	public static function view($file, $values = null, $extension = 'php') {
		
		$cboiler['option'] = Page::get_opt_values($file);
		$cboiler['option_data'] = Page::get_page_data($cboiler['option']['name']);
		
		$helper = new Helper();
		$helper->set_option_name($cboiler['option']);
		$helper->set_option_group($cboiler['option']);
		$helper->set_option_values($cboiler['option_data']);
		
		$asset = new Assets();
		
		if (is_array($values) && !is_null($values)) {
			foreach ($values as $key => $value) {
				$$key = $value;					
			}
		}
		
		require  self::get_file_path($file, 'View', $extension);

	}
	
	
	/* Get asset file path
	 * 
     * @param $folder string
     * @param $file string
     * @param $extension string
     *
     * @return void
     *
     */
    private static function get_asset_url($folder, $file, $extension) {

		$compliment = 'assets' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $file . $extension;
		$file_path = PLUGINDEFINE_PATH . $compliment;
		
		return (file_exists($file_path)) ? PLUGINDEFINE_URL . '/' . $compliment : $file ;

	}
	
	/* Get script file to enqueue
	 * 
     * @param $file string
     * @param $extension string
     *
     * @return void
     *
     */
    public static function script($script) {
		
		return self::get_asset_url('script', $script, false);
	
	}
	
	/* Get style file to enqueue
	 * 
     * @param $file string
     * @param $extension string
     *
     * @return void
     *
     */
    public static function style($style) {
		
		return self::get_asset_url('style', $style, false);
	
	}
	
	/* Get image file
	 * 
     * @param $file string
     * @param $extension string
     *
     * @return void
     *
     */
    public static function image($img, $extension = false) {
		
		$extension = ($extension === false)?'':'.'.$extension;
		return self::get_asset_url('img', $img, $extension);
	
	}
	
	
}
