<?php
namespace PluginNameSpace\Core\Lib;

use PluginNameSpace\Core\Lib\Menu;
use PluginNameSpace\Core\Lib\Assets;
use PluginNameSpace\Core\Lib\Controller;

class Plugin extends Pimple {
    
    /**
     * Call basic functions to run Plugin
     *
     * @return void
     */	
    public function run()
    {
        foreach( $this->values as $key => $content ){ // Loop on contents
            $content = $this[$key];
            
            if( is_object( $content ) ){
                $reflection = new \ReflectionClass( $content );
                if( $reflection->hasMethod( 'run' ) ){
                    $content->run(); // Call run method on object
                }
            }
        }
        
        //Load Assets
        $assets = new Assets();

        //Load Menu
        if (is_admin()) {
            $menu_items = Load::config('menu');
            if ($menu_items !== false) {
                $menu = new Menu($menu_items);
                $menu->load_menu();
            }
        }
        
    }

    /**
     * Call controller
     *
     * @param $controller string Controller Name
     * @param $where string admin or user (if both or something different, will load in both sides)
     *
     * @return void
     */	    
    public function call_controller($controller, $where = 'both')
    {
        
        if (empty($controller)) return;
        
        $class = new Controller;
        if (strtolower($where) == 'admin') {
            if (is_admin()) return $class->do_new($controller);
        } else if (strtolower($where) == 'user') {
            if (!is_admin()) return $class->do_new($controller);
        } else {
            return $class->do_new($controller);
        }
        
    }
    
    /**
     * Call translation files
     *
     * @param string $folder
     */
    public function call_translation($folder = 'languages')
    {
        load_plugin_textdomain( PLUGINDEFINE_LANG, FALSE, PLUGINDEFINE_BASENAME . '/'.$folder.'/' );
    }
}
