<?php
namespace PluginNameSpace\Core\Lib;

class General {
	
	public static function get_class($caller_id = 3) {
		$method = self::get_calling_function($caller_id);
		$method = explode('\\', $method['class']);
		return array_pop($method);
	}
	
    /**
     * Get function and class that has called History
     *
     * @return json
     */
    private static function get_calling_function($caller_id) {
        $caller = debug_backtrace();
        $caller = $caller[$caller_id];
        $r['function'] = $caller['function'];
        
        if (isset($caller['class'])) {
            $r['class'] = $caller['class'];
        }
        
        if (isset($caller['object'])) {
            $r['object'] = get_class($caller['object']);
        }
        return $r;
    }
}
