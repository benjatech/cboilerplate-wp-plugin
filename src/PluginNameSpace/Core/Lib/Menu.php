<?php
namespace PluginNameSpace\Core\Lib;

class Menu extends Pimple {

	/* Call WP function to create a Menu on Admin page and register settings
	 * 
     * @param $file string
     *
     * @return action
     *
     */
    public function load_menu() {
		add_action( 'admin_menu', array( $this, 'add_menu_and_page' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}
	
	/* Create menus and submenus on Admin Page using standard WP Function
     *
     * @return void
     *
     */
    public function add_menu_and_page() { 
		
        foreach( $this->values as $key => $page ) {
			
			$controller = ( isset ($page['controller'] ) ) ? $page['controller'] : $key;
			$function = ( isset( $page['function'] ) ) ? $page['function'] : 'render_page';
			
            if( array_key_exists( 'parent_slug', $page ) ) { //Verify if it is a sub-page
                add_submenu_page(
                    $page['parent_slug'],
                    $page['page_title'],
                    $page['menu_title'],
                    $page['capability'],
                    $page['menu_slug'],
                    array( '\\'. PLUGINDEFINE_NAMESPACE .'\Controller\\' . $controller, $function )
                );					
            } else {
                add_menu_page(
                    $page['page_title'],
                    $page['menu_title'],
                    $page['capability'],
                    $page['menu_slug'],
                    array( '\\'. PLUGINDEFINE_NAMESPACE .'\Controller\\' . $controller, $function ),
                    $page['icon_url'],
                    $page['position']
                );
            }
        }
        
    }
	
	/* Register settings using standard WP Function
     *
     * @return void
     *
     */
	
    public function register_settings() { 
    
		foreach( $this->values as $page ) {
			if( isset( $page['option_group'] ) && isset( $page['option_name'] ) ) {
				register_setting(
					$page['option_group'],
					$page['option_name']
				);
			}
		}
		
    }
}
