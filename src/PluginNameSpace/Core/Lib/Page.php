<?php
namespace PluginNameSpace\Core\Lib;

class Page extends Pimple {
	
    public function get_page_data($option_name){
        return get_option( $option_name, self::get_default_page_data() );
    }
	
	public function get_default_page_data() {
		$defaults = array();
		
		return $defaults;
	}
	
	public function get_menu_values($menu_id) {
		$menu = Load::config('menu');
		return $menu[$menu_id];
	}
	
	public function get_opt_values($class = false) {
	  	$menu = self::get_menu_values((!$class)?General::get_class():$class);
		$opt = [
				'group' => $menu['option_group'],
				'name'	=> $menu['option_name']
			   ];
		
		return $opt;
	}
}
