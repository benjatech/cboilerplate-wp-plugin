# cBoiler - A Quick Start for your new WordPress Plugin
---

This boiler plate is simple. It doesn't intent to reinvent the wheel (neither any Wordpress function). It just try to make it easier and faster to start developing a plugin without waste time with boring task!

If you have any idea on how to make this boilerplate better, tell me: mauro.baptista@carnou.com

For full documentation, check: http://cboiler.carnou.com/